package exams.second;
//name: Yirui Wei ;	 course(cs201); sec.#(03)
//date: 04/30/2022;

public abstract class Polygon {

    private String name;

    public Polygon() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Polygon{" +
                "name='" + name + '\'' +
                '}';
    }

    public double area(){
        return 0d;
    }

    public double perimeter(){
        return 0d;
    }
}
