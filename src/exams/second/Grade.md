# Final Exam

## Total

68/100

## Break Down

1. Inheritance/Polymorphism:    9/20
    - Superclass:               0/5
    - Subclass:                 0/5
    - Variables:                5/5
    - Methods:                  4/5
2. Abstract Classes:            5/20
    - Superclass:               0/5
    - Subclasses:               0/5
    - Variables:                5/5
    - Methods:                  0/5
3. ArrayLists:                  19/20
    - Compiles:                 4/5
    - ArrayList:                5/5
    - Exits:                    5/5
    - Results:                  4/5
4. Sorting Algorithms:          20/20
    - Compiles:                 5/5
    - Selection Sort:           10/10
    - Results:                  5/5
5. Searching Algorithms:        15/20
    - Compiles:                 5/5
    - Jump Search:              5/10
    - Results:                  5/5

## Comments

1. Both super class and subclass are wrong. -10.
   The program didn't check the validation of seats. -1
2. area and perimeter should be abastract. -5. 
   Rectange and Circle should be subclass. -5. 
   Methods are not implemented. -5
3. Error when first input is 'done'. -1 
4. ok
5. Didn't implement the jump search algorithm with recursively. -5
