package exams.second;
//name: Yirui Wei ;	 course(cs201); sec.#(03)
//date: 04/30/2022; 


import java.util.ArrayList;
import java.util.Scanner;

public class QuestionThree {
    public static void main(String[] args) {
        Boolean flag = true;
        ArrayList<Integer> list = new ArrayList<>();
        while (flag){
            System.out.println("Please enter a sequence of numbers");
            Scanner input = new Scanner(System.in);
            String num = input.next();
            if ("Done".equals(num)){
                flag = false;
            }else {
                list.add(Integer.valueOf(num));
            }
        }
        int max = list.get(0);
        int min = list.get(0);

        for (int i=1;i<list.size();i++) {
            if (min > list.get(i)){
                min = list.get(i);
            }
            if (max < list.get(i)){
                max = list.get(i);
            }
        }
        for (Integer integer : list) {
            System.out.println(integer);
        }
        System.out.println("minimum:"+min+",max:"+max);  //output minimum
    }
}
