package exams.second;
//name: Yirui Wei ;	 course(cs201); sec.#(03)
//date: 04/30/2022;

public class ComputerLab {

    private Boolean computers;

    public ComputerLab() {
    }

    public void setComputers(Boolean computers) {
        this.computers = computers;
    }

    public Boolean hasComputers(){
        return true;
    }

    @Override
    public String toString() {
        return "ComputerLab{" +
                "computers=" + computers +
                '}';
    }
}
