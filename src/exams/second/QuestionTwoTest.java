package exams.second;
//name: Yirui Wei ;	 course(cs201); sec.#(03)
//date: 04/30/2022; 
import java.util.Scanner;
public class QuestionTwoTest {
	public static void main(String[] args) {
        Boolean flag = true;
        while (flag){
            Circle circle = new Circle();
            Rectangle rectangle = new Rectangle();

            System.out.println("1.Area and perimeter of a circle");
            System.out.println("2.area and perimeter of a rectangle");
            System.out.println("3.exit");
            Scanner scanner = new Scanner(System.in);
            String next = scanner.next();
            if ("1".equals(next)){
                System.out.println("Please input the circle radius");
                Scanner inputRadius = new Scanner(System.in);
                Double radius = inputRadius.nextDouble();
                if (radius == null){
                    radius = circle.getRadius();
                }else if (radius < 0){
                    System.out.println("Please enter a positive number");
                    return;
                }

                double area = Math.PI * radius * radius;
                double perimeter = 2.0 * Math.PI * radius;
                System.out.println(circle.getName()+"area:"+area+","+circle.getName()+"perimeter:"+perimeter);
            }else if ("2".equals(next)){
                System.out.println("Please enter height");
                Scanner inputHeight = new Scanner(System.in);
                Double height = inputHeight.nextDouble();
                if (height == null){
                    height = rectangle.getHeight();
                }else if (height < 0){
                    System.out.println("Please enter a positive number");
                    return;
                }
                System.out.println("Please enter width");
                Scanner inputRadius = new Scanner(System.in);
                Double width = inputRadius.nextDouble();
                if (width == null){
                    width = rectangle.getWidth();
                }else if (width < 0){
                    System.out.println("Please enter a positive number");
                    return;
                }
                double area = width * height;
                double perimeter = 2.0 * (width * height);
                System.out.println(rectangle.getName()+"area:"+area+","+rectangle.getName()+"perimeter:"+perimeter);
            }else if ("3".equals(next)){
                flag = false;
            }
        }
    }
}
