package exams.second;
//name: Yirui Wei ;	 course(cs201); sec.#(03)
//date: 04/30/2022;

public class Classroom extends ComputerLab{

    private String Building;
    private String roomNumber;
    private int seats;

    public Classroom() {
    }

    public String getBuilding() {
        return Building;
    }

    public void setBuilding(String building) {
        Building = building;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    @Override
    public String toString() {
        return "Classroom{" +
                "Building='" + Building + '\'' +
                ", roomNumber='" + roomNumber + '\'' +
                ", seats=" + seats +
                '}';
    }
}
