package exams.second;
//name: Yirui Wei ;	 course(cs201); sec.#(03)
//date: 04/30/2022;
import java.util.Scanner;

public class QuestionFive {
    public static void main(String[] args) {
        Double[] list = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
        System.out.println("Please input a programming language ");
        Scanner scanner = new Scanner(System.in);
        Double val = scanner.nextDouble();
        int jumpSearch = jumpSearch(list, val);

        if(jumpSearch>=0) {
            System.out.println(val + " is found at index " + jumpSearch + ".");
        }else {
            System.out.println(val + " is not found at Programming Language Array " +  ".");
        }
    }

    public static int jumpSearch(Double[] list, Double val) {

        int arrayLength = list.length;
        int jumpStep = (int) Math.sqrt(list.length);
        int previousStep = 0;

        while (list[Math.min(jumpStep, arrayLength) - 1] < val) {
            previousStep = jumpStep;
            jumpStep += (int)(Math.sqrt(arrayLength));
            if (previousStep >= arrayLength) {
                return -1;
            }
        }
        while (list[previousStep] < val) {
            previousStep++;
            if (previousStep == Math.min(jumpStep, arrayLength)) {
                return -1;
            }
        }

        if (list[previousStep].equals(val)) {
            return previousStep;
        }
        return -1;
    }
}
