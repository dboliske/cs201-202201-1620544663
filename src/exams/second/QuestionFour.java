
package exams.second;
//name: Yirui Wei ;	 course(cs201); sec.#(03)
//date: 04/30/2022;
import java.util.Arrays;

//name: Yirui Wei ;	 course��cs201��; sec.#��03��
//date: 04/30/2022;
public class QuestionFour {
    public static void main(String[] args) {
        String[] list = {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"};
        System.out.println("before sorting"+Arrays.toString(list));
        list = selection(list);
        System.out.println("after sorting"+Arrays.toString(list));

    }

    public static String[] selection(String[] strings){

        for(int i = 0; i < strings.length - 1; i++) {// do the i-th sort
            int k = i;
            for(int j = k + 1; j < strings.length; j++){// pick the smallest record
                if(strings[j].compareTo(strings[k]) < 0){
                    k = j; //Take note of where the smallest value found so far is located
                }
            }

            if(i != k){  //exchange a[i] and a[k]
                String temp = strings[i];
                strings[i] = strings[k];
                strings[k] = temp;
            }
        }
        return strings;
    }
}
