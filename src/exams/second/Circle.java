package exams.second;
//name: Yirui Wei ;	 course(cs201); sec.#(03)
//date: 04/30/2022;

public class Circle extends Polygon{

    private double radius;

    public Circle() {
        this.radius = 1;
        this.setName("圆形");
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                '}';
    }

    public double area(){
        return 0d;
    }

    public double perimeter(){
        return 0d;
    }
}
