package exams.second;
//name: Yirui Wei ;	 course(cs201); sec.#(03)
//date: 04/30/2022;

public class Rectangle extends Polygon{

    private double width;
    private double height;

    public Rectangle() {
        this.width = 1d;
        this.height = 1d;
        this.setName("rectangle");
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "width=" + width +
                ", height=" + height +
                '}';
    }

    public double area(){
        return 0d;
    }

    public double perimeter(){
        return 0d;
    }
}
