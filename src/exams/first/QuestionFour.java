package exams.first;

//name: Yirui Wei ;	 course��cs201��; sec.#��03��
//date: 02/25/2022;  name of project:Array for Question Four

/*
 * Write a program that prompts the user for 5 words and prints out any word that appears more than once. 
 * NOTE: 
 * The words should be an exact match, i.e. it should be case sensitive.
 * Your programmustuse an array.
 * Do not sort the data or use an ArrayLists.
 */

import java.util.Scanner;

public class QuestionFour {
    public static void main(String[] args) {

        String[] array = new String[5];
        Scanner scanner = new Scanner(System.in);
        for(int i=0; i<array.length; i++) {
            // prompt user to input a word
            System.out.println("Please input a word: ");
            // record the word to array
            array[i] = scanner.nextLine();
        }
        String[] repeat = new String[5];
        for (int i=0;i<array.length;i++) {
            for (int j=0;j<array.length;j++) {
                if (i == j){
                    continue;
                }
                if (array[i].equals(array[j])){
                    if (repeat[j]!=null && repeat[j].equals(array[i])){
                        continue;
                    }
                    repeat[i] = array[i];
                }
            }
        }
        System.out.println("User input words are::");
        for (int i=0;i<array.length;i++) {
            System.out.print(array[i]+"  ");
        }
        System.out.println();
        System.out.println("Repeated words are:");
        for (int i=0;i<repeat.length;i++) {
            if (repeat[i]!=null){
                System.out.print(repeat[i]+"  ");
            }
        }
        scanner.close();
    }
}
