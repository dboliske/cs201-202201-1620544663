package exams.first;
//name: Yirui Wei ;	 course��cs201��; sec.#��03��
//date: 02/25/2022;  name of project:Selection for Question One

//Write a program that prompts the user for an integer, add 65 to it, convert the result to a character and print that character to the console.

import java.util.Scanner;
public class QuestionOne {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Please input an integer");
        int val = scanner.nextInt();
        int ascii = 65;

        int ascii2 = val + ascii;

        if (ascii2 > 126 || ascii2 < 33){
            System.out.println("Please input an integer between -32 and 61 ");
            System.exit(0);
        }
        char as = (char) ascii2;
        System.out.println(val + " convert the result to a character is " + as);
        scanner.close();
    }
}
