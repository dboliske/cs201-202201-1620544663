package exams.first;
//name: Yirui Wei ;	 course��cs201��; sec.#��03��
//date: 02/25/2022;  name of project:Data Types for Question Two

/*
 * Write a program that prompts the user for an integer. 
 * If the integer is divisible by 2 print out "foo", 
 * and if the integer is divisible by 3 print out "bar".
 * If the integer is divisible by both, your program should print out "foobar" 
 * and if the integer is not divisible by either, then your program should not print out anything.
 */
import java.util.Scanner;

public class QuestionTwo {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Please input an integer");
        int integer = scanner.nextInt();

        if (integer % 2 == 0 && integer % 3 == 0){
            System.out.println("foobar");
        }else if (integer % 2 == 0){
            System.out.println("foo");
        }else if (integer % 3 == 0){
            System.out.println("bar");
        }else {
            System.out.println();
        }
        scanner.close();
    }
}
