package exams.first;
//name: Yirui Wei ;	 course��cs201��; sec.#��03��
//date: 02/25/2022;  name of project:Repetition for Question Three

//Write a program that prompts the user for an integer and then prints out a Triangle of that height and width. For example, if the user enters 3, then your program should print the following:

import java.util.Scanner;
public class Questionthree {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please input an integer");
        int val = scanner.nextInt();
        for (int i = val;i >= 1;i--){
            for (int j = 1;j <= val - i;j++){
                System.out.print(" ");
            }
            for (int j = 1;j <= i;j++){
                System.out.print("*");
            }
            System.out.println();
        }
        scanner.close();
    }
}
