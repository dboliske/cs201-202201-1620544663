package exams.first;
//name: Yirui Wei ;	 course��cs201��; sec.#��03��
//date: 02/25/2022;  name of project: Objects for Question Five

/*
 * Write a Java class based on the following UML diagram. Include all standard methods, such as constructors, mutators, accessors, toString, and equals. Additionally, implement any other methods shown in the diagram.
 */

public class Pet {

	    // Create two instance variables, name (a String) and strength (a double)
	    private String name;
	    private int age;

	    // Write the default constructor.
	    public Pet() {
	        name = "bindundun";
	        age = 1;
	    }

	    //Write the non-default constructor.
	    public Pet(String name, int age) {
	        setName(name);
	        setAge(age);
	    }


	    //Write two mutator methods
	    public void setName(String name) {
	        if (name!=null || name!="") {
	            this.name = name;
	        }
	    }

	    public void setAge(int age) {
	        if (age > 0) {
	            this.age = age;
	        }
	    }

	    // Write two accessor methods, one for each instance variable.
	    public String getName() {
	        return name;
	    }
 
	    public int getAge() {
	        return age;
	    }

	    // Write a method that will compare this instance to another pet
	    public boolean equals(Object obj) {
	        if (obj instanceof Pet) {
	        	Pet pet = (Pet)obj;
		        if (!(this.name.equals(pet.name))) {
		            return false;
		        } 
		        if (this.age != pet.age) {
		            return false;
		        }
		        return true;
	        }
	        return false;
	    }

	    // Write  the toString method
	    public String toString() {
	        return this.name + "is the pet's name," + "the age of the pet is"+this.age ;
	    }
}
