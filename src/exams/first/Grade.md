# Midterm Exam

## Total

94/100

## Break Down

1. Data Types:                  18/20
    - Compiles:                 5/5
    - Input:                    3/5
    - Data Types:               5/5
    - Results:                  5/5
2. Selection:                   18/20
    - Compiles:                 5/5
    - Selection Structure:      10/10
    - Results:                  3/5
3. Repetition:                  18/20
    - Compiles:                 5/5
    - Repetition Structure:     5/5
    - Returns:                  5/5
    - Formatting:               3/5
4. Arrays:                      20/20
    - Compiles:                 5/5
    - Array:                    5/5
    - Exit:                     5/5
    - Results:                  5/5
5. Objects:                     20/20
    - Variables:                5/5
    - Constructors:             5/5
    - Accessors and Mutators:   5/5
    - toString and equals:      5/5

## Comments

1. Good, but doesn't confirm that the user's input is an integer.
2. Same issue as Question 1.
3. Same issue as Questions 1 and 2.
4. Good
5. Good
