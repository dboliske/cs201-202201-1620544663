package project;
//name: Yirui Wei ;	 course��cs201��; sec.#��03��
//date: 04/27/2022;  name of project:Project


public class UserInfoProduct {


    private Long userInfoId;


    private Long productId;

    public UserInfoProduct(Long userInfoId, Long productId) {
        this.userInfoId = userInfoId;
        this.productId = productId;
    }

    @Override
    public String toString() {
        return "UserInfoProduct{" +
                "userInfoId=" + userInfoId +
                ", productId=" + productId +
                '}';
    }

    public Long getUserInfoId() {
        return userInfoId;
    }

    public void setUserInfoId(Long userInfoId) {
        this.userInfoId = userInfoId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }
}
