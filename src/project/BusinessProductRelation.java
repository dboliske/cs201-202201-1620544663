package project;
//name: Yirui Wei ;	 course��cs201��; sec.#��03��
//date: 04/27/2022;  name of project:Project

/**
 * Merchant and product association
 */
public class BusinessProductRelation {

    /**
     * Merchant ID
     */
    private Long businessId;

    /**
     * Product ID
     */
    private Long productId;

    public BusinessProductRelation(Long businessId, Long productId) {
        this.businessId = businessId;
        this.productId = productId;
    }

    @Override
    public String toString() {
        return "BusinessProductRelation{" +
                "businessId=" + businessId +
                ", productId=" + productId +
                '}';
    }

    public Long getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Long businessId) {
        this.businessId = businessId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }
}
