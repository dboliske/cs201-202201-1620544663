package project;
//name: Yirui Wei ;	 course��cs201��; sec.#��03��
//date: 04/27/2022;  name of project:Project

public class UserInfo {


    private Long userInfoId;


    private String name;


    private int age;

    public UserInfo() {
    }

    public UserInfo(Long userInfoId, String name, int age) {
        this.userInfoId = userInfoId;
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "userInfoId=" + userInfoId +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public Long getUserInfoId() {
        return userInfoId;
    }

    public void setUserInfoId(Long userInfoId) {
        this.userInfoId = userInfoId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
