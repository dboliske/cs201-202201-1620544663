package project;

//name: Yirui Wei ;	 course��cs201��; sec.#��03��
//date: 04/27/2022;  name of project:Project

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

public class a {

    public static void main(String[] args) {

        int num = 0;
        List<UserInfo> users = new ArrayList<>();
        List<Product> products = new ArrayList<>();
        List<Business> businesses = new ArrayList<>();
        List<UserInfoProduct> userInfoProducts = new ArrayList<>();
        List<BusinessProductRelation> businessProductRelations = new ArrayList<>();
        Boolean isFlag = true;
        while (isFlag){
            System.out.println("1.User menu");
            System.out.println("2.Product menu");
            System.out.println("3.Purchase");
            System.out.println("4.Order Tracking");
            System.out.println("5.Quit");
            System.out.println();
            System.out.println("Please input your option!");
            Scanner input = new Scanner(System.in);
            String next = input.next();
            if ("1".equals(next)){
                user(users);
            }else if ("2".equals(next)){
                product(num,businesses,products,businessProductRelations);
            }else if ("3".equals(next)){
                purchase(users,products,userInfoProducts);
            }else if ("4".equals(next)){
                order(users,products,userInfoProducts);
            }else {
                isFlag = false;
            }
        }
    }

    public static void order(List<UserInfo> users, List<Product> products, List<UserInfoProduct> userInfoProducts){
        if (isEmpty(users) || isEmpty(products)){
            System.out.println("No users or products yet");
            return;
        }
        if (isEmpty(userInfoProducts)){
            System.out.println("No order information yet");
            return;
        }
        System.out.println("Please enter a registered user name");
        Scanner userInput = new Scanner(System.in);
        String user = userInput.next();
        UserInfo info = new UserInfo();
        for (UserInfo userInfo : users) {
            if (userInfo.getName().equals(user)){
                info = userInfo;
                break;
            }
        }
        if (info.getUserInfoId() == null){
            System.out.println("No users yet");
            return;
        }
        UserInfo finalInfo = info;
        List<UserInfoProduct> infoProducts = userInfoProducts.stream().filter(o -> o.getUserInfoId().equals(finalInfo.getUserInfoId())).collect(Collectors.toList());
        for (UserInfoProduct infoProduct : infoProducts) {
            for (Product product : products) {
                if (infoProduct.getProductId().equals(product.getProductId())){
                    System.out.println("User:"+finalInfo.getName()+",Purchased as:"+product.getName()+",The unit price is:"+product.getPrice());
                }
            }
        }
    }

    public static void purchase(List<UserInfo> users, List<Product> products, List<UserInfoProduct> userInfoProducts){

        if (isEmpty(users) || isEmpty(products)){
            System.out.println("No users or products yet");
            return;
        }
        System.out.println("Please enter a registered user name");
        Scanner userInput = new Scanner(System.in);
        String user = userInput.next();
        UserInfo info = new UserInfo();
        for (UserInfo userInfo : users) {
            if (userInfo.getName().equals(user)){
                info = userInfo;
                break;
            }
        }
        if (info.getUserInfoId() == null){
            System.out.println("No users yet");
            return;
        }
        System.out.println("The following are the items that can be purchased, enter the serial number to purchase");
        for (Product product : products) {
            System.out.println(product.getProductId()+".product name:"+product.getName()+",commodity price:"+product.getName()+",Commodity stocks:"+product.getStock());
        }
        Scanner userProductInput = new Scanner(System.in);
        int userProduct = userProductInput.nextInt();

        for (Product product : products) {
            if (Long.valueOf(userProduct).equals(product.getProductId())){
                product.setStock(product.getStock() - 1);
                UserInfoProduct userInfoProduct = new UserInfoProduct(info.getUserInfoId(),product.getProductId());
                userInfoProducts.add(userInfoProduct);
            }
        }
        System.out.println("Purchase successfully");
    }

    public static void product(int num, List<Business> businesses, List<Product> products, List<BusinessProductRelation> businessProductRelations) {

        Business business = new Business();
        System.out.println("Please input a business name");
        Scanner businessInput = new Scanner(System.in);
        String businessName = businessInput.next();
        if (!isEmpty(businesses)){
            for (Business businessShop : businesses) {
                if (businessShop.getName().equals(businessName)){
                    business = businessShop;
                }
            }
        }else {
            num = num + 1;
            business = new Business(Long.valueOf(num),businessName);
            businesses.add(business);
        }
        if (business.getBusinessId() == null){
            Business business1 = businesses.get(businesses.size() - 1);
            business = new Business(Long.valueOf(business1.getBusinessId() + 1),businessName);
            businesses.add(business);
        }
        Boolean isFlag = true;
        while (isFlag){

            System.out.println("1. Product list");
            System.out.println("2. Add product");
            System.out.println("3. Delete product");
            System.out.println("4. Search for products");
            System.out.println("5. Modify the product");
            System.out.println("6. Exit");
            System.out.println();
            System.out.println("Please input your option!");
            Scanner input = new Scanner(System.in);
            String next = input.next();
            if ("1".equals(next)){
                listProduct(business,products,businessProductRelations);
            }else if ("2".equals(next)){
                addProduct(business,products,businessProductRelations);
            }else if ("3".equals(next)){
                deleteProduct(business,products,businessProductRelations);
            }else if ("4".equals(next)){
                searchProduct(products);
            }else if ("5".equals(next)){
                updateProduct(products);
            }else if ("6".equals(next)){
                isFlag = false;
            }
        }
    }

    public static void listProduct(Business business,List<Product> products,List<BusinessProductRelation> businessProductRelations){
        Long businessId = business.getBusinessId();
        List<BusinessProductRelation> productRelations = businessProductRelations.stream().filter(o -> o.getBusinessId().equals(businessId)).collect(Collectors.toList());
        List<Long> productIds = productRelations.stream().map(BusinessProductRelation::getProductId).collect(Collectors.toList());
        ArrayList<Product> productArrayList = new ArrayList<>();
        for (Long productId : productIds) {
            for (Product product : products) {
                if (productId.equals(product.getProductId())){
                    productArrayList.add(product);
                }
            }
        }
        System.out.println(productArrayList);
    }

    public static void addProduct(Business business,List<Product> products,List<BusinessProductRelation> businessProductRelations){

        int num = 0;
        if (!products.isEmpty()){
            num = Integer.valueOf(String.valueOf(products.get(products.size() - 1).getProductId()));
        }

        System.out.println("1.csv import");
        System.out.println("2.Merchant input");
        System.out.println();
        System.out.println("Please input your option!");
        Scanner input = new Scanner(System.in);
        String next = input.next();
        if ("1".equals(next)){
            List<Product> productList = readFile(num,"./src/project/stock.csv");
            if (!isEmpty(productList)) {
                products.addAll(productList);
                for (Product product : productList) {
                    BusinessProductRelation businessProductRelation = new BusinessProductRelation(business.getBusinessId(), product.getProductId());
                    businessProductRelations.add(businessProductRelation);
                }
            }
        }else if ("2".equals(next)){

            System.out.println("Please input product name");
            Scanner productNameScanner = new Scanner(System.in);
            String productName = productNameScanner.next();
            System.out.println("Please input product price");
            Scanner productPriceScanner = new Scanner(System.in);
            String productPrice = productPriceScanner.next();
            System.out.println("Please input product inventory");
            Scanner productStockScanner = new Scanner(System.in);
            int productStock = productStockScanner.nextInt();
            Product product = new Product(Long.valueOf(num + 1),productName,Double.valueOf(productPrice),productStock);
            products.add(product);
            BusinessProductRelation businessProductRelation = new BusinessProductRelation(business.getBusinessId(), product.getProductId());
            businessProductRelations.add(businessProductRelation);
        }
        System.out.println("ProductionInfo Saved Successfully");
        System.out.println();
    }

    public static void deleteProduct(Business business,List<Product> products,List<BusinessProductRelation> businessProductRelations){
        Long businessId = business.getBusinessId();
        List<BusinessProductRelation> productRelations = businessProductRelations.stream().filter(o -> o.getBusinessId().equals(businessId)).collect(Collectors.toList());
        List<Long> productIds = productRelations.stream().map(BusinessProductRelation::getProductId).collect(Collectors.toList());
        ArrayList<Product> productArrayList = new ArrayList<>();
        for (Long productId : productIds) {
            for (Product product : products) {
                if (productId.equals(product.getProductId())){
                    productArrayList.add(product);
                }
            }
        }
        System.out.println("Please input the delete item ID");
        Scanner productScanner = new Scanner(System.in);
        Long productId = productScanner.nextLong();
        List<Product> productList = productArrayList.stream().filter(o -> o.getProductId().equals(productId)).collect(Collectors.toList());
        for (Product product : productList) {
            products.remove(product);
            BusinessProductRelation businessProductRelation = new BusinessProductRelation(businessId, productId);
            businessProductRelations.remove(businessProductRelation);
        }
        System.out.println("successfully deleted");
    }

    public static void searchProduct(List<Product> products){
        System.out.println("Please input product name");
        Scanner productNameScanner = new Scanner(System.in);
        String productName = productNameScanner.next();
        List<Product> productList = products.stream().filter(o -> o.getName().contains(productName)).collect(Collectors.toList());
        System.out.println(productList);
    }

    public static void updateProduct(List<Product> products){
        System.out.println("Please input to modify the product ID");
        Scanner productScanner = new Scanner(System.in);
        Long productId = productScanner.nextLong();
        List<Product> productList = products.stream().filter(o -> o.getProductId().equals(productId)).collect(Collectors.toList());
        if (isEmpty(productList)){
            System.out.println("No data");
            return;
        }
        Product product = productList.get(0);
        System.out.println(product);
        System.out.println("Please input product name");
        Scanner productNameScanner = new Scanner(System.in);
        String productName = productNameScanner.next();
        System.out.println("Please input product price");
        Scanner productPriceScanner = new Scanner(System.in);
        String productPrice = productPriceScanner.next();
        System.out.println("Please input product inventory");
        Scanner productStockScanner = new Scanner(System.in);
        int productStock = productStockScanner.nextInt();
        Product product1 = new Product(product.getProductId(),productName,Double.valueOf(productPrice),productStock);
        products.remove(product);
        products.add(product1);
        System.out.println("Successfully modified");
    }

    public static void user(List<UserInfo> users) {

        Boolean isFlag = true;
        while (isFlag){

            System.out.println("1. List function");
            System.out.println("2. Add function");
            System.out.println("3. Delete function");
            System.out.println("4. Search function");
            System.out.println("5. Modify function");
            System.out.println("6. Exit");
            System.out.println();
            System.out.println("Please input your option!");
            Scanner input = new Scanner(System.in);
            String next = input.next();
            if ("1".equals(next)){
                listUser(users);
            }else if ("2".equals(next)){
                addUser(users);
            }else if ("3".equals(next)){
                deleteUser(users);
            }else if ("4".equals(next)){
                searchUser(users);
            }else if ("5".equals(next)){
                updateUser(users);
            }else if ("6".equals(next)){
                isFlag = false;
            }
        }
    }

    public static void listUser(List<UserInfo> users){
    	System.out.println("User information is as follow:");
        System.out.println(users);

    }

    public static void addUser(List<UserInfo> users){
        int num = 0;
        System.out.println("Please enter username");
        Scanner userNameScanner = new Scanner(System.in);
        String userName = userNameScanner.next();
        System.out.println("Please enter user age");
        Scanner userAgeScanner = new Scanner(System.in);
        int userAge = userAgeScanner.nextInt();
        if (!users.isEmpty()){
            num = Integer.valueOf(String.valueOf(users.get(users.size() - 1).getUserInfoId()));
        }
        UserInfo userInfo = new UserInfo(Long.valueOf(num + 1),userName,userAge);
        users.add(userInfo);

        System.out.println("UserInfo Successfully saved");
    }

    public static void deleteUser(List<UserInfo> users){
        System.out.println("Please enter delete user ID");
        Scanner userInfoIdScanner = new Scanner(System.in);
        Long userInfoId = userInfoIdScanner.nextLong();
        List<UserInfo> userInfoList = users.stream().filter(o -> o.getUserInfoId().equals(userInfoId)).collect(Collectors.toList());
        for (UserInfo userInfo : userInfoList) {
            users.remove(userInfo);
        }
        System.out.println("UserInfo successfully deleted");
    }

    public static void searchUser(List<UserInfo> users){
        System.out.println("Please enter username");
        Scanner userNameScanner = new Scanner(System.in);
        String userName = userNameScanner.next();
        List<UserInfo> userInfoList = users.stream().filter(o -> o.getName().contains(userName)).collect(Collectors.toList());
        System.out.println(userInfoList);
    }

    public static void updateUser(List<UserInfo> users){
        System.out.println("Please enter the modified user ID");
        Scanner userInfoIdScanner = new Scanner(System.in);
        Long userInfoId = userInfoIdScanner.nextLong();
        List<UserInfo> userInfoList = users.stream().filter(o -> o.getUserInfoId().equals(userInfoId)).collect(Collectors.toList());
        if (isEmpty(userInfoList)){
            System.out.println("No data");
            return;
        }
        UserInfo userInfo = userInfoList.get(0);
        System.out.println(userInfo);
        System.out.println("Please enter username");
        Scanner userNameScanner = new Scanner(System.in);
        String userName = userNameScanner.next();
        System.out.println("Please enter user age");
        Scanner userAgeScanner = new Scanner(System.in);
        int userAge = userAgeScanner.nextInt();
        UserInfo user = new UserInfo(userInfo.getUserInfoId(),userName,userAge);
        users.remove(userInfo);
        users.add(user);
        System.out.println("Successfully modified");
    }

    //readFile
    public static List<Product> readFile(int num, String filename) {
        List<Product> products = new ArrayList<>();
        int i = 0;
        try {
            File f1 = new File(filename);
            Scanner f = new Scanner(f1);
            while (f.hasNextLine()) {
                String l = f.nextLine();
                if (l.split(",")[0].equals("Name")) {
                    continue;
                } else {
                    String[] xy = l.split(",");
                    String name = xy[0];
                    Product product = new Product(Long.valueOf(num + 1), name, Double.valueOf(xy[1]), 1);
                    products.add(product);
                }
            }
        }catch (Exception e) {
            System.out.println("Read Error!");
        }

        List<Product> productArrayList = new ArrayList<>();
        List<String> list = products.stream().map(Product::getName).distinct().collect(Collectors.toList());
        Map<String, List<Product>> listMap = products.stream().collect(Collectors.groupingBy(Product::getName));
        for (String name : list) {
            int size = listMap.get(name).size();
            List<Product> productList = products.stream().filter(o -> o.getName().equals(name)).distinct().collect(Collectors.toList());
            Product product = productList.get(0);
            num = num + 1;
            Product product1 = new Product(Long.valueOf(num), name, product.getPrice(), size);
            productArrayList.add(product1);
        }
        return productArrayList;
    }
    
    public static boolean isEmpty(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }
}