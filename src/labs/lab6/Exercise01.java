package labs.lab6;
//name: Yirui Wei ;	 course��cs201��; sec.#��03��
//date: 03/20/2022;  name of project:Exercise01
import java.util.ArrayList;
import java.util.Scanner;
public class Exercise01 {
    public static void main(String[] args) {

        Boolean flag = true;
        ArrayList<String> customerList = new ArrayList<>();
        while (flag){
        	//menu
            System.out.println("1. Add customer to queue");
            System.out.println("2. Help customer");
            System.out.println("3. Exit");

            Scanner scanner = new Scanner(System.in);

            int num = scanner.nextInt();
            switch (num){
                case 1:
                    int addCustomerToQueue = addCustomerToQueue(customerList);
                    System.out.println("user in queue"+addCustomerToQueue+"Location");
                    break;
                case 2:
                    String helpCustomer = helpCustomer(customerList);
                    System.out.println(helpCustomer);
                    break;
                case 3:
                    flag = false;
                    break;
            }
        }
    }

    private static int addCustomerToQueue(ArrayList<String> customerList){
        System.out.println("Please enter username");
        Scanner sa = new Scanner(System.in);
        customerList.add(sa.nextLine());
        return customerList.size() - 1;
    }

    private static String helpCustomer(ArrayList<String> customerList){

        try {
            if (customerList.size()==0){
                return "There are no customers in the queue";
            }else {
                return customerList.remove(0);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
