package labs.lab4;
//name: Yirui Wei ;	 course��cs201��; sec.#��03��
//date: 02/21/2022;  name of project:GeoLocation

public class PhoneNumber {
	//1.Create three instance variables, countryCode, areaCode and number, all of which should be Strings.
	private String countryCode;
	private String areaCode;
	private String number;
	
	//2.Write the default constructor.
	public PhoneNumber()
	{
		countryCode = "+86";
		areaCode = "021";
		number = "1433731";
	}
	//3.Write the non-default constructor.
	public PhoneNumber(String countryCode, String areaCode, String number)
	{
		this.countryCode = countryCode;
		this.areaCode = areaCode;
		this.number = number;
	}
	//4.Write 3 accessor methods, one for each instance variable.
	public String getCountryCode()
	{
		return countryCode;
	}

	public String getAreaCode()
	{
		return areaCode;
	}

	public String getNumber()
	{
		return number;
	}
	
	//5.Write 3 mutator methods, one for each instance variable.
	public void setCountryCode(String countryCode)
	{
		this.countryCode = countryCode;
	}
	public void setAreaCode(String areaCode)
	{
		
		this.areaCode = areaCode;
	
	}

	public void setNumber(String number)
	{
		
		this.number = number;
		
	}
	//6.Write a method that will return the entire phone number as a single string (the toString method).
	public String toString()
	{
		return countryCode + "-" + areaCode + " " + number;
	}
	//7.Write a method that will return true if the areaCode is 3 characters long.
	public boolean vaildAreaCode()
	{
		if (areaCode.length() == 3)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	//8.Write a method that will return true if the number is 7 characters long
	public boolean vaildNumber()
	{
		if (number.length() == 7)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	//9.Write a method that will compare this instance to another PhoneNumber (the equals method).
	public boolean equals(PhoneNumber p)
	{
//		if (this.toString() == p.toString())
//		{
//			return true;
//		}
//		else {
//			return false;
//		}
		if (this.countryCode == p.countryCode)
		{
			return true;
		}
		else if (this.areaCode == p.areaCode)
		{
			return true;
		}
		else if (this.number == p.number)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}	
}
