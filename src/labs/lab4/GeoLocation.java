package labs.lab4;
//name: Yirui Wei ;	 course��cs201��; sec.#��03��
//date: 02/21/2022;  name of project:GeoLocation

public class GeoLocation {

		//1.Create two instance variables, lat and lng, both of which should be doubles.
		private double lat;
		private double lng;
		
		//2.Write the default constructor.
		public GeoLocation()
		{
			lat = 0.0;
			lng = 0.0;
		}
		//3.Write the non-default constructor.
		public GeoLocation(double lat, double lng)
		{
			setLat(lat);
			setLng(lng);
		}
		
		// 4.Write 2 accessor methods, one for each instance variable
		public double getLat()
		{
			return lat;
		}
	
		public double getLng()
		{
			return lng;
		}
		
		// 5.Write 2 mutator methods, one for each instance variable.
		public void setLat(double lat)
		{
			this.lat = lat;
		}
		
		public void setLng(double lng)
		{
			this.lng = lng;
		}
		
		// 6.Write a method that will return the location in the format "(lat, lng)" (the toString method).
		public String toString() 
		{
			return "(%s,%s)".formatted(lat,lng);
		}
		
		//7.Write a method that will return true if the latitude is between -90 and +90.
		public boolean validLat(double lat)
		{
			if (lat >= -90 && lat <= 90)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		//8.Write a method that will return true if the longitude is between -180 and +180.
		public boolean validLng(double lng)
		{
			if (lng >= -180 && lng <= 180)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		//9.Write a method that will compare this instance to another GeoLocation (the equals method).
	    public boolean equals(GeoLocation o) {
	        if (this == o) {
	        	return true;
	        }
	        if (o == null || getClass() != o.getClass()) {
	        	return false;
	        }
	        return Double.compare(o.lat, lat) == 0 && Double.compare(o.lng, lng) == 0;
	    }
		
	}



