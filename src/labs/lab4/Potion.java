package labs.lab4;
//name: Yirui Wei ;	 course��cs201��; sec.#��03��
//date: 02/21/2022;  name of project:GeoLocation
public class Potion {
	//1.Create two instance variables, name (a String) and strength (a double).
		private String name;
		private double strength;
		
		//2.Write the default constructor.
		public Potion()
		{
			name = "bingdundun";
			strength = 1.0d;
		}
		//3.Write the non-default constructor.
		public Potion(String name, double strength)
		{
			this.name = name;
			this.strength = strength;
		}
		
		//4.Write two accessor methods, one for each instance variable.
		public String getName()
		{
			return name;
		}
		public double getStength()
		{
			return strength;
		}
		
		//5.Write two mutator methods, one for each instance variable.
		public void setName(String name)
		{
			this.name = name;
		}
		public void setStrength(double strength)
		{
			this.strength = strength;
			
		}
		//6.Write a method that will return the entire as a single string (the toString method).
		public String toString()
		{
			return "The name is: " + name + ", "+"The strength is: " + strength;
		}
		//7.Write a method that will return true if the strength is between 0 and 10.
		public boolean vaildStrength(double strength)
		{
			if (strength >= 0 && strength <= 10)
			{
				return true;
			} 
			else 
			{
				return false;
			}
		}
		//8.Write a method that will compare this instance to another Potion (the equals method).
		public boolean equals(Potion p)
		{
			if (this.name == p.name)
			{
				return true;
			}
			else if(this.strength == p.strength)
			{
				return true;
			}
			else 
			{
				return false;
			}
		}
}
