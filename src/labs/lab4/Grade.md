# Lab 4

## Total

24/30

## Break Down

* Part I GeoLocation
  * Exercise 1-9        7/8
  * Application Class   1/1
* Part II PhoneNumber
  * Exercise 1-9        5/8
  * Application Class   1/1
* Part III 
  * Exercise 1-8        6/8
  * Application Class   1/1
* Documentation         3/3

## Comments

Good, but your GeoLocation doesn't validate in the mutators, your PhoneNumber doesn't validate in either the non-default constructor or mutator and doesn't follow the UML diagram for the validation methods, and your Potion class doesn't validate in either the non-default constructor or mutator.
