
package labs.lab4;
//name: Yirui Wei ;	 course（cs201）; sec.#（03）
//date: 02/21/2022;  name of project:GeoLocation
/*
10.Now write an application class that instantiates two instances of GeoLocation.
 One instance should use the default constructor and the other should use the non-default constructor. 
 Display the values of the instance variables by calling the accessor methods.
*/
public class TestGeoLocation {

	public static void main(String[] args) {
		// default constructor
		GeoLocation GeoLocation = new GeoLocation(); 
		// non-default constructor
		GeoLocation GeoLocation2 = new GeoLocation(2.8, -6.9);
		// 无参构造 No-argument constructor
		System.out.println(GeoLocation.toString());
		GeoLocation.setLat(2.8);
		System.out.println(GeoLocation.toString());
		GeoLocation.setLng(-6.9);
		System.out.println(GeoLocation.toString());
		
		// 有参构造 parameterized construction
		System.out.println(GeoLocation2.toString());
		//result:false
		GeoLocation2.setLat(0);
		System.out.println(GeoLocation.equals(GeoLocation2));
		//result:true
		GeoLocation2.setLat(2.8);
		System.out.println(GeoLocation.equals(GeoLocation2));
		//result:False
		GeoLocation2.setLng(-5.0);
		System.out.println(GeoLocation.equals(GeoLocation2));
	}

}
