package labs.lab4;
//name: Yirui Wei ;	 course��cs201��; sec.#��03��
//date: 02/21/2022;  name of project:GeoLocation

public class TestPhoneNumber {
/*
 * 10.Now write an application class that instantiates two instances of PhoneNumber. 
 * One instance should use the default constructor and the other should use the non-default constructor. 
 * Display the values of each object by calling the toString method.	
 */
	
	public static void main(String[] args) {

		PhoneNumber PhoneNumber = new PhoneNumber();
		System.out.println("The PhoneNumber1: " + PhoneNumber.toString());
		
		PhoneNumber PhoneNumber2 = new PhoneNumber("+86", "888", "55511291");
		System.out.println("The PhoneNumber2: " + PhoneNumber2.toString());
		
		//result:true
		System.out.println("the compare result: "+PhoneNumber.equals(PhoneNumber2));
		//result:true
		System.out.println("the compare result: "+PhoneNumber.vaildAreaCode());
		//result:false
		System.out.println("the compare result: "+PhoneNumber2.vaildNumber());
			
	}
}
