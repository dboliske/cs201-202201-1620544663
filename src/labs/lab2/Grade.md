# Lab 2

## Total

15/20

## Break Down

* Exercise 1    5/6
* Exercise 2    5/6
* Exercise 3    5/6
* Documentation 0/2

## Comments

Code is mostly good, but you have infinite loops in _all_ of your programs. Additionally, there is no documentation for any of the programs.
