package labs.lab2;

/**
 * 
 * @author yirui wei
 * course  CS201
 * date 02/06/2022
 * This program will prompt the user for a number and print out a square with those dimensions.
 * For example, if the user enters 3, return the following:
         *  *  *  
         *     *  
         *  *  * 
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class squarewithdimensions {
    public static void main(String[] args) {

        for (;;){
            BufferedReader bu = new BufferedReader(new InputStreamReader(System.in));

            String str = null;

            System.out.println("Please enter a number:");

            try {
                str = bu.readLine();

            } catch (IOException e) {
                e.printStackTrace();

            }
            int num = Integer.valueOf(str);
            for (int i=1; i<=num;i++){
                for (int j=1; j<=num;j++){
                    if (i==1 || i==num){
                        System.out.print("*  ");
                    }else if (j==1 || j==num){
                        System.out.print("*  ");
                    }else if (i!=1 && i!=num && j!=1 && j!=num){
                        System.out.print("   ");
                    }
                }
                System.out.println();
            }
        }
    }
}
