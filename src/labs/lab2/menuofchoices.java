package labs.lab2;

/**
 * 
 * @author yirui wei
 * course  CS201
 * date 02/06/2022
 * This program is about that a menu of choices.
 * 1.Say Hello - This should print "Hello" to console.
 * 2.Addition - This should prompt the user to enter 2 numbers and return the sum of the two.
 * 3.Multiplication - This should prompt the user to enter 2 numbers and return the product of the two.
 * 4.Exit - Leave the program
 */


import java.math.BigDecimal;
import java.util.Scanner;

public class menuofchoices {

    public static void main(String[] args) {

        System.out.println("==a menu of choices ==");
        System.out.println("1.Say Hello");
        System.out.println("2.Addition");
        System.out.println("3.Multiplication");
        System.out.println("4.Exit");
        for (;;){
            Scanner scanner = new Scanner(System.in);

            int nextInt = scanner.nextInt();
            if (1==nextInt){
                System.out.println("Hello");
                System.out.println("1.-----execution ends------");
            }else if (2==nextInt){
                System.out.println("Please enter the first number");
                Scanner s1 = new Scanner(System.in);
                BigDecimal one = s1.nextBigDecimal();

                System.out.println("Please enter a second number");
                Scanner s2 = new Scanner(System.in);
                BigDecimal two = s2.nextBigDecimal();
                System.out.println("the sum of the two:" +one.add(two));
                System.out.println("2.-----execution ends------");
            }else if (3==nextInt){
                System.out.println("Please enter the first number");
                Scanner s1 = new Scanner(System.in);
                BigDecimal one = s1.nextBigDecimal();

                System.out.println("Please enter a second number");
                Scanner s2 = new Scanner(System.in);
                BigDecimal two = s2.nextBigDecimal();

                System.out.println("the product of the two: " + one.multiply(two));
                System.out.println("3.-----execution ends------");
            }else if (4==nextInt){
                System.exit(0);
            }
        }
    }
}
