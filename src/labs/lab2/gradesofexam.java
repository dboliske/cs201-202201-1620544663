package labs.lab2;

/**
 * 
 * @author yirui wei
 * course  CS201
 * date 02/06/2022
 * This program is about that the grades for an exam.
 * Note: If the user enter value:-1 when the program will finish entering grades
 */

import java.math.BigDecimal;
import java.util.Scanner;

public class gradesofexam {

    public static void main(String[] args) {

        for (;;){
            Scanner scanner = new Scanner(System.in);
            System.out.println("Please enter physical grades");
            BigDecimal language = scanner.nextBigDecimal();
            Scanner scanner1 = new Scanner(System.in);
            System.out.println("Please enter math score");
            BigDecimal mathematics = scanner1.nextBigDecimal();

            BigDecimal sum = language.add(mathematics);
            BigDecimal avg = language.add(mathematics).divide(new BigDecimal("2")).setScale(2,BigDecimal.ROUND_HALF_UP);
            System.out.println("Your overall score:"+sum+"��your average score:"+avg);
        }
    }
}