package labs.lab7;
//name: Yirui Wei ;	 course��cs201��; sec.#��03��
//date: 04/04/2022;  name of project:Binary Search Recursion
//1.Write a Java program that will implement the Binary Search algorithm as a recursive method and be able to search the following array of Strings for a specific value, input by the user:
//{"c", "html", "java", "python", "ruby", "scala"}

import java.util.Scanner;

public class BinarySearchRecursion {

    public static void main(String[] args) {
        String[] srcArray = {"c", "html", "java", "python", "ruby", "scala"};
        System.out.println("Please input a programming language ");
        Scanner scanner = new Scanner(System.in);
        String val = scanner.nextLine();
        int binSearch = binSearch(srcArray, 0, srcArray.length - 1, val);
 //       System.out.println(binSearch);
        if(binSearch>=0) {
            System.out.println(val + " is found at index " + binSearch + ".");
        }else {
        	System.out.println(val + " is not found at Programming Language Array " +  ".");
        }
    }

    // Recursive implementation of binary search
    public static int binSearch(String srcArray[], int start, int end, String key) {
        int mid = (end + start) / 2;
        if (start > end) {
            return -1;
        }
        if (srcArray[mid].equals(key)) {
            return mid;
        } else if (srcArray[mid].compareTo(key) < 0) {
            return binSearch(srcArray, mid + 1, end, key);
        } else if (srcArray[mid].compareTo(key) > 0) {
            return binSearch(srcArray, start, mid - 1, key);
        }
        return -1;
    }
}
