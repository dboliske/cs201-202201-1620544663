package labs.lab7;
//name: Yirui Wei ;	 course��cs201��; sec.#��03��
//date: 04/04/2022;  name of project:Bubble Sort Algorithms
//1.Write a Java program that will implement the Bubble Sort algorithm as a method and sort the following array of integers:
//{10, 4, 7, 3, 8, 6, 1, 2, 5, 9}

import java.util.Arrays;

public class BubbleSortAlgorithms {
	 public static void main(String[] args) {
	        int[] array = {10, 4, 7, 3, 8, 6, 1, 2, 5, 9};
	        System.out.println(array);
	        array = bubble(array);
	        System.out.println(Arrays.toString(array));
	    }

	    public static int[] bubble(int[] array){

	        int temp;
	        for (int i = 0;i<array.length;i++){
	            for (int j = 0;j<array.length - i - 1;j++){
	                if(array[j+1]<array[j]){
	                    temp = array[j];
	                    array[j] = array[j+1];
	                    array[j+1] = temp;
	                }
	            }
	        }
	        return array;
	    }
}
