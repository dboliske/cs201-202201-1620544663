package labs.lab7;
//name: Yirui Wei ;	 course��cs201��; sec.#��03��
//date: 04/04/2022;  name of project:Selection Sort Algorithms
//1.Write a Java program that will implement the Selection Sort algorithm as a method and sorts the following array of doubles:
//{3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282}


import java.util.Arrays;

public class SelectionSort {
	public static void main(String[] args) {
        double[] array = {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};
        array = selection(array);
        System.out.println(Arrays.toString(array));
    }

    public static double[] selection(double[] doubles){

        for(int i = 0; i < doubles.length - 1; i++) {// do the i-th sort 
            int k = i;
            for(int j = k + 1; j < doubles.length; j++){// pick the smallest record
                if(doubles[j] < doubles[k]){
                    k = j; //Take note of where the smallest value found so far is located
                }
            }
            
            if(i != k){  //exchange a[i] and a[k]
                double temp = doubles[i];
                doubles[i] = doubles[k];
                doubles[k] = temp;
            }
        }
        return doubles;
    }
}
