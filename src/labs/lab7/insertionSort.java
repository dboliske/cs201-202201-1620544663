package labs.lab7;

//name: Yirui Wei ;	 course��cs201��; sec.#��03��
//date: 04/04/2022;  name of project:insertion Sort Algorithms
//1.Write a Java program that will implement the Insertion Sort algorithm as a method and sort the following array of Strings:
//{"cat", "fat", "dog", "apple", "bat", "egg"}

import java.util.Arrays;


public class insertionSort {
	   public static void main(String[] args) {
	        String[] str = {"cat", "fat", "dog", "apple", "bat", "egg"};
	        str = insert(str);
	        System.out.println(Arrays.toString(str));
	    }

	    public static String[] insert(String[] str){

	        int i,j;
	        for (i = 0;i<str.length;i++) {
	            String temp = str[i];
	            for(j=i-1;j>=0 && str[j].compareTo(temp) > 0;j--){
	                str[j+1] = str[j];
	            }
	            str[j+1] = temp;
	        }
	        return str;
	    }
}
