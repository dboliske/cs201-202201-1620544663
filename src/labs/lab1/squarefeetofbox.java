package labs.lab1;

/**
 * 
 * @author yirui wei
 * course  CS201
 * date 02/06/2022
 * This program is about that Calculate the amount of wood which is needed to make the box.
 * note:the user should enter the values provided below
 */


	import java.math.BigDecimal;
	import java.util.Scanner;

	public class squarefeetofbox {

	    public static void main(String[] args) {
	        Scanner s1 = new Scanner(System.in);
	        System.out.println("Please enter an optional length value��20,30,40,50,60");
	        System.out.println("Please enter an optional width value��10,20,30,40,50");
	        System.out.println("Please enter an optional depth value��10,30,40,50");
	        System.out.println("Please enter an optional length value(inches),width value(inches),depth value(inches),(such as��2,1,1)");
	        String number = s1.next();
	        String[] split = number.split(",");
	        Double length = new Double(split[0]);
	        Double width = new Double(split[1]);
	        Double depth = new Double(split[2]);
            /*
             * Calculate the surface area of a cube
             */
	        Double lengthWidth = length*width;
	        Double lengthDepth = length*depth;
	        Double widthDepth = width*depth;
	        Double sum = lengthWidth+lengthDepth+widthDepth;
	        Double aDouble = 2*sum;
	        double num = aDouble / 12 /12;
	        BigDecimal bigDecimal1 = new BigDecimal(num);
	        BigDecimal bigDecimal = bigDecimal1.setScale(2,BigDecimal.ROUND_HALF_UP);
	        System.out.println("the amount of wood needed is " + bigDecimal + " square feet");
	    }
	}



