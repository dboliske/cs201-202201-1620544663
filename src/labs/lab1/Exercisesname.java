package labs.lab1;
/**
 * 
 * @author yirui wei
 * course  CS201
 * date 02/06/2022
 * This program that will prompt a user for a name; save the input and echo the name to the console.
 */

import java.util.Scanner;

public class Exercisesname {
	public static void main(String[] args) {
	    Scanner username1 = new Scanner(System.in);
	    System.out.println("what's your name");
	    String username = username1.nextLine();
	    System.out.println(username);
	}
}
