package labs.lab1;

/**
 * 
 * @author yirui wei
 * course  CS201
 * date 02/06/2022
 * This program is about that how the Fahrenheit and Celsius convert.
 * note:the user should enter the values provided below
 */

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class temperatures {
	public static void main(String[] args) {
        List<List<String>> list = new ArrayList<>();
        List<String> list1 = new ArrayList<>();
        List<String> list2 = new ArrayList<>();
        List<String> list3 = new ArrayList<>();
        String q1 = "low temperatures";
        String q2 = "-2";
        String q3 = "-1";
        String q4 = "-0.5";
        String q5 = "0";
        String q6 = "1";
        String q7 = "2";
        String w1 = "middle temperatures";
        String w2 = "10";
        String w3 = "11";
        String w4 = "11.5";
        String w5 = "12";
        String w6 = "12.1";
        String w7 = "15";
        String e1 = "Hight  temperatures";
        String e2 = "28";
        String e3 = "29";
        String e4 = "29.5";
        String e5 = "30";
        String e6 = "31";
        String e7 = "32";
        list1.add(q1);
        list1.add(q2);
        list1.add(q3);
        list1.add(q4);
        list1.add(q5);
        list1.add(q6);
        list1.add(q7);
        list2.add(w1);
        list2.add(w2);
        list2.add(w3);
        list2.add(w4);
        list2.add(w5);
        list2.add(w6);
        list2.add(w7);
        list3.add(e1);
        list3.add(e2);
        list3.add(e3);
        list3.add(e4);
        list3.add(e5);
        list3.add(e6);
        list3.add(e7);
        list.add(list1);
        list.add(list2);
        list.add(list3);
        for (List<String> strings : list) {
            for (String string : strings) {
                System.out.print(string+"   ");
            }
            System.out.println();
        }
        
        /* arithmetic calculation about: convert the Fahrenheit to Celsius.
         */
        Scanner s1 = new Scanner(System.in);
        System.out.println("Please input temperatures in Fahrenheit");
        String temperature = s1.next();
        common(list, temperature);

        Double c = 5 * (Double.valueOf(temperature) - 32)/9;
        BigDecimal decimal = new BigDecimal(c);
        decimal = decimal.setScale(6,BigDecimal.ROUND_HALF_UP);
        System.out.println(temperature+" Fahrenheit to Celsius is "+decimal);

        System.out.println("--------------------------");
        
        /* arithmetic calculation about: convert the Celsius to Fahrenheit.
         */
        Scanner s2 = new Scanner(System.in);
        System.out.println("Please input temperatures in Celsius");
        String centigrade = s2.next();
        common(list, centigrade);

        Double f = Double.valueOf(centigrade)*9/5+32;
        BigDecimal decimal1 = new BigDecimal(f);
        decimal1 = decimal1.setScale(6,BigDecimal.ROUND_HALF_UP);
        System.out.println(centigrade+" Celsius to Fahrenheit is "+decimal1);
    }

    private static void common(List<List<String>> list, String num){
        Boolean flag = true;
        for (List<String> strings : list) {
            if (strings.contains(num)){
                flag = false;
            }
        }

        if (flag){
            System.out.println("The user should enter the values which provided");
            System.exit(0);
        }
    }
}
