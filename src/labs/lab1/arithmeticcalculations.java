package labs.lab1;

/**
 * 
 * @author yirui wei
 * course  CS201
 * date 02/06/2022
 * This program is about something arithmetic calculations.
 */

import java.math.BigDecimal;
import java.util.Scanner;

public class arithmeticcalculations {


	    public static void main(String[] args) {
	        Scanner s1 = new Scanner(System.in);
	        System.out.println("what is your father��s age");
	        BigDecimal father = s1.nextBigDecimal();
	        Scanner s2 = new Scanner(System.in);
	        System.out.println("what is your age");
	        BigDecimal yours = s2.nextBigDecimal();
            /* arithmetic calculation about: Your age subtracted from your father's age.
             */
	        BigDecimal disparity = father.subtract(yours);
	        System.out.println("Your age subtracted from your father's age is "+disparity);

	        System.out.println("------------------");
            /* arithmetic calculation about: Your birth year multiplied by 2.
             */
	        Scanner s3 = new Scanner(System.in);
	        System.out.println("Please input your birth year");
	        BigDecimal year = s3.nextBigDecimal();
	        System.out.println("Your birth year multiplied by 2 is " +year.multiply(new BigDecimal(2)));

	        System.out.println("------------------");
            /* arithmetic calculation about: Convert your height in inches to cms.
             */
	        Scanner s4 = new Scanner(System.in);
	        System.out.println("Please input your height(inches)");
	        BigDecimal height = s4.nextBigDecimal();
	        System.out.println("your height is "+height.multiply(new BigDecimal(2.54)).setScale(2,BigDecimal.ROUND_HALF_UP) +" cms");

	        System.out.println("------------------");
            /* arithmetic calculation about: Convert your height in inches to feet and inches where inches is an integer.
             */
	        Scanner s5 = new Scanner(System.in);
	        System.out.println("Please input your height(inches),such as 65,66,67,68,etc");
	        int height2 = s5.nextInt();
	        BigDecimal bigDecimal = new BigDecimal(height2);
	        System.out.println("your height is "+bigDecimal.divide(new BigDecimal(12),2,BigDecimal.ROUND_HALF_UP) +" feet");

	        System.out.println("------------------");  
	        /*Getting a char from keyboard input:Prompt a user for a first name; display the user's first initial to the screen.
	         */
	        Scanner s6 = new Scanner(System.in);
	        System.out.println("what is your first name?");
	        char firstName = s6.next().charAt(0);
	        System.out.println(firstName);
	    }
}
