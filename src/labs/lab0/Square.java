package labs.lab0;
/**
 * 
 * @author yirui wei
 * course  CS201
 * date 27/01/2022
 * The name of the program is Square
 *
 */
public class Square {

	public static void main(String[] args) {
        for (int i = 0 ; i < 5 ; i++){
            for (int j = 0 ; j < 10 ; j++){
                if (i==0 || i==4){
                    if (j>=5){
                        break;
                    }
                    System.out.print("* ");
                }else if (j==0 || j==7){
                    System.out.print("* ");
                }else if (j!=0 || j!=7){
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
	}

}
