package labs.lab3;
//name: Yirui Wei ;	 course��cs201��; sec.#��03��
//date: 02/23/2022;  name of project: Min

//Write a program that will find the minimum value and print it to the console for the given array:
public class Min {
    public static void main(String[] args) {
        //the given array:
        int[] ints = {72, 101, 108, 108, 111, 32, 101, 118, 101, 114, 121, 111, 110, 101, 33, 32, 76, 111, 111, 107, 32, 97, 116, 32, 116, 104, 101, 115, 101, 32, 99, 111, 111, 108, 32, 115, 121, 109, 98, 111, 108, 115, 58, 32, 63264, 32, 945, 32, 8747, 32, 8899, 32, 62421};
        int init = ints[0];

        for (int i=1;i<ints.length;i++) {
            if (init > ints[i]){
                init = ints[i];
            }
        }
        System.out.println("minimum:"+init);  //output minimum
    }
}
