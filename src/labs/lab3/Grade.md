# Lab 3

## Total

11/20

## Break Down

* Exercise 1    3/6
* Exercise 2    3/6
* Exercise 3    3/6
* Documentation 2/2

## Comments

This lab is testing your knowledge of arrays; in all 3 exercises, you used ArrayLists, which are a completely different data structure. ~~Also, again, there is no documentation in any of the programs.~~
