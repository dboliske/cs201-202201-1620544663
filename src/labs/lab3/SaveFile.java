package labs.lab3;

/**
 * 
 * @author yirui wei
 * course  CS201
 * date 02/14/2022
 */
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class SaveFile {
	public static void main(String[] args) {

        ArrayList<String> list = new ArrayList<>();
        for (;;){
            Scanner scanner = new Scanner(System.in);
            String next = scanner.next();
            if ("Done".equals(next)){
                break;
            }
            list.add(next);
        }
        System.out.println("Please input a file name");
        Scanner scanner = new Scanner(System.in);
        String next = scanner.next();

        //File file=new File("D:/"+next+".txt");  //output file to this path
        File file=new File("./src/labs/lab3/"+next+".txt");
        try {

            if (!file.exists())
                file.createNewFile();
            FileOutputStream out = new FileOutputStream(file, true);

            for (String s1:list) {
                StringBuffer sb = new StringBuffer();
                sb.append(s1+" ");

                out.write(sb.toString().getBytes("utf-8"));
            }
            out.close();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
