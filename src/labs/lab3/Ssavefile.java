package labs.lab3;

//name: Yirui Wei ;	 course��cs201��; sec.#��03��
//date: 02/23/2022;  name of project: Ssavefile

/*Write a program that will continue to prompt the user for numbers, storing them in an array, until they enter "Done" to finish, 
 * then prompts the user for a file name so that these values can be saved to that file. For example,
 * if the user enters "output.txt", then the program should write the numbers that have been read to "output.txt".
 */

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;

public class Ssavefile {
	public static void main(String[] args) {

		int num = 0;
        Double[] number = new Double[10];
        for (;;){
        	System.out.println("Please enter the value randomly, if you want to end the value input, please enter Done. ");
            Scanner scanner = new Scanner(System.in);
            String next = scanner.next();
            if ("Done".equals(next)){
                break;
            }
            number[num] = Double.valueOf(next);
            num++;
        }
        System.out.println("Please name a file name");
        Scanner scanner = new Scanner(System.in);
        String next = scanner.next();

        //File file=new File("D:/"+next+".txt");  //output file to this path
        File file=new File("./src/labs/lab3/"+next+".txt");
        try {

            if (!file.exists()) {
                file.createNewFile();
            }
            BufferedWriter out = new BufferedWriter(new FileWriter(file));

            for(int i=0; i < number.length; i++) {
            	if(number[i] == null) {
            		break;
            	}
                out.write(String.valueOf(number[i]) + " ");
            }
            out.close();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
