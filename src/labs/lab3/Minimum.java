package labs.lab3;

/**
 * 
 * @author yirui wei
 * course  CS201
 * date 02/14/2022
 */

import java.util.Arrays;

public class Minimum {
	public static void main(String[] args) {
        Integer[] ints = {72, 101, 108, 108, 111, 32, 101, 118, 101, 114, 121, 111, 110, 101, 33, 32, 76, 111, 111, 107, 32, 97, 116, 32, 116, 104, 101, 115, 101, 32, 99, 111, 111, 108, 32, 115, 121, 109, 98, 111, 108, 115, 58, 32, 63264, 32, 945, 32, 8747, 32, 8899, 32, 62421};
        Integer min = Arrays.stream(ints).min(Integer::compare).get(); //Take the minimum value for comparison
        System.out.println(min);  //output minimum
        String s = Arrays.toString(ints);
        System.out.println(s);  //output the array
    }
}
