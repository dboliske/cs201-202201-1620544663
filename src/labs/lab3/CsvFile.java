package labs.lab3;

//name: Yirui Wei ;	 course��cs201��; sec.#��03��
//date: 02/23/2022;  name of project: CsvFile
/*1.You have been given a file called "src/labs/lab3/grades.csv". 
 * It contains a list of students and their exam grades.
 * Write a program that reads in the file and computes the average grade for the class and then prints it to the console.
 */
import java.io.IOException;
import java.io.*;

public class CsvFile {

	public static void main(String[] args) {
		File csvFile = new File("./src/labs/lab3/grades.csv");
        String line = "";
        String cvsSplitBy = ",";
        Double sum = 0.0d;
        int num = 0;

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] country = line.split(cvsSplitBy);
                
                sum = sum + Double.valueOf(country[1]);

                System.out.println("name: " + country[0] + " , grade: " + country[1] + " ");
                num++;

            }
            Double average = sum / num;  //Calculate the average score of class
            System.out.println("The average score of class is��"+average);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
