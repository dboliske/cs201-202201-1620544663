package labs.lab3;

/**
 * 
 * @author yirui wei
 * course  CS201
 * date 02/14/2022
 */

import java.io.IOException;
import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;

public class CSVFILEREAD {

	public static void main(String[] args) {
		File csvFile = new File("./src/labs/lab3/grades.csv");
        String line = "";
        String cvsSplitBy = ",";
        ArrayList<BigDecimal> grades = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] country = line.split(cvsSplitBy);

                BigDecimal grade = new BigDecimal(country[1]);
                grades.add(grade);

                System.out.println("name: " + country[0] + " , grade: " + country[1] + " ");

            }
            long count = grades.stream().count();
            BigDecimal sum = BigDecimal.ZERO;
            for (BigDecimal gra : grades){
                sum = sum.add(gra);
            }
            BigDecimal divide = sum.divide(new BigDecimal(count), 2, BigDecimal.ROUND_HALF_UP);  //Calculate class average
            System.out.println("The average score of class is��"+divide);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
