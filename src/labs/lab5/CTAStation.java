package labs.lab5;
//name: Yirui Wei ;	 course��cs201��; sec.#��03��
//date: 03/07/2022;  name of CTAStation extends GeoLocation
//Create a class that will implement a CTAStation, which should inherit from GeoLocation
//Demonstrate knowledge of how to use super() in constructors

public class CTAStation extends GeoLocation {


	private String name;
	private String location;
	private boolean wheelchair;
	private boolean open;
	
    //CTAStation()
	public CTAStation() {
		super();
		name = "California";
		location = "elevated";
		wheelchair = true;
		open = true;
	}
	
    //CTAStation(name String ,lat double ,lng double ,location String ,wheelchair boolean , open boolean
	public CTAStation(String name, double lat, double lng, String location,boolean wheelchair, boolean open) {
		super(lat, lng);
		this.name = name;
		this.location = location;
		this.wheelchair = wheelchair;
		this.open = open;
	}
	

	public String getName() {
		return name;
	}
	
	public String getLocation() {
		return location;
	}
	
	public boolean hasWheelchair() {
		return wheelchair;
	}
	
	public boolean isOpen() {
		return open;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public void setWheelchair(boolean wheelchair) {
		this.wheelchair = wheelchair;
	}
	
	public void setOpen(boolean open) {
		this.open = open;
	}
	
	public String toString() {
		return "The Name is: " + name + " , the Location is: " + location + " Wheelchiar: " + wheelchair + ",  Open: " + open;
	}
	
	//
	public boolean equals(Object o) {
		if (!(o instanceof GeoLocation)) {
			return false;
		} 
		GeoLocation g = (GeoLocation)o;
		if(!(super.equals(g))) {
			return false;
		}else if (!(o instanceof CTAStation)) {
			return false;
		}
		CTAStation c = (CTAStation)o;
		if(!(this.name.equals(c.name))) {
			return false;
		} else if(!(this.location.equals(c.location))) {
			return false;
		} else if(this.hasWheelchair()!= c.hasWheelchair()) {
			return false;
		} else if(this.isOpen() != c.isOpen()) {
			return false;
		} else {
			return true;
		}
	}
	

	
}
