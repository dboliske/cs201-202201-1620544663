package labs.lab5;
//name: Yirui Wei ;	 course��cs201��; sec.#��03��
//date: 03/07/2022;  name of GeoLocation
//Add to your GeoLocation class from last week to include the following:
//1.A method called calcDistance that takes another GeoLocation and returns a double. Note: This should use the formula Math.sqrt(Math.pow(lat1 - lat2, 2) + Math.pow(lng1 - lng2, 2)).
//2.A method called calcDistance that takes a lng and lat and returns a double. Note: See above formula.


public class GeoLocation {
	//1.Create two instance variables, lat and lng, both of which should be doubles.
	private double lat;
	private double lng;
	
	//2.Write the default constructor.
	public GeoLocation()
	{
		lat = 0.0;
		lng = 0.0;
	}
	//3.Write the non-default constructor.
	public GeoLocation(double lat, double lng)
	{
		setLat(lat);
		setLng(lng);
	}
	
	// 4.Write 2 accessor methods, one for each instance variable
	public double getLat()
	{
		return lat;
	}

	public double getLng()
	{
		return lng;
	}
	
	//--��Ҫ��֤
	// 5.Write 2 mutator methods, one for each instance variable.
	public void setLat(double lat)
	{
		this.lat = lat;
	}
	
	public void setLng(double lng)
	{
		this.lng = lng;
	}
	
	// 6.Write a method that will return the location in the format "(lat, lng)" (the toString method).
	public String toString() 
	{
		return "(%s,%s)".formatted(lat,lng);
	}
	
	//7.Write a method that will return true if the latitude is between -90 and +90.
	public boolean validLat(double lat)
	{
		if (lat >= -90 && lat <= 90)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	//8.Write a method that will return true if the longitude is between -180 and +180.
	public boolean validLng(double lng)
	{
		if (lng >= -180 && lng <= 180)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	//9.Write a method that will compare this instance to another GeoLocation (the equals method).
    public boolean equals(GeoLocation g) {
        if (this == g) {
        	return true;
        }
        if (g == null || getClass() != g.getClass()) {
        	return false;
        }
        return Double.compare(g.lat, lat) == 0 && Double.compare(g.lng, lng) == 0;
    }
    
	//A method called calcDistance that takes another GeoLocation and returns a double.
	public double calcDistance(GeoLocation g) {
		// This should use the formula Math.sqrt(Math.pow(lat1 - lat2, 2) + Math.pow(lng1 - lng2, 2)).
		return Math.sqrt(Math.pow(this.lat - g.lat, 2) + Math.pow(this.lng - g.lng, 2));
	}
	
	//A method called calcDistance that takes a lng and lat and returns a double. Note: See above formula
	public double calcDistance(double lat, double lng) {
		// This should use the formula Math.sqrt(Math.pow(lat1 - lat2, 2) + Math.pow(lng1 - lng2, 2)).
		return Math.sqrt(Math.pow(this.lat - lat, 2) + Math.pow(this.lng - lng, 2));
	}
    
    
}
