# Lab 5

## Total

16/20

## Break Down

CTAStation

- Variables:                    2/2
- Constructors:                 1/1
- Accessors:                    2/2
- Mutators:                     2/2
- toString:                     1/1
- equals:                       2/2

CTAStopApp

- Reads in data:                2/2
- Loops to display menu:        0/2
- Displays station names:       1/1
- Displays stations by access:  2/2
- Displays nearest station:     0/2
- Exits                         1/1

## Comments
- Didn't implement loops to display menu. -2
- Didn't calculate the nearest station and display. -2