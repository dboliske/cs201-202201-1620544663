package labs.lab5;
//name: Yirui Wei ;	 course��cs201��; sec.#��03��
//date: 03/07/2022;  name of CTAStopApp
//This application class will display a menu of options that the user can select from to display stations that meet certain criteria. 
//The UML diagram for the class is as shown below, but here is the general description for each method:

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class CTAStopApp {
	
	// Reads stations from the input file and stores the data in an instance of CTAStation[].
	public static void main(String[] args) {	
		//main:Calls readFile to load the data
		CTAStation[] fileStation = readFile("src/labs/lab5/CTAStops.csv");
		if (fileStation[0] != null) {
			//Passes that data to menu
			menu(fileStation);
		}
	}
	//readFile
	//Reads stations from the input file and stores the data in an instance of CTAStation[].
	public static CTAStation[] readFile(String filename) {
		CTAStation[] stations = new CTAStation[12];
		int i = 0;
		try {
			File f1 = new File(filename);
			Scanner f = new Scanner(f1);
			while(f.hasNextLine()) {
				String l = f.nextLine();
				String[] values = l.split(",");
				if (l.split(",")[0].equals("Name")) {
					continue;
				} else {
					String[] xy = l.split(",");
					String name = xy[0];
					double lat = Double.parseDouble(xy[1]);
					double lng = Double.parseDouble(xy[2]);
					String location = xy[3];
					boolean wheelchair = Boolean.parseBoolean(xy[4]);
					boolean open = Boolean.parseBoolean(xy[5]);
					CTAStation L = new CTAStation(name, lat, lng, location, wheelchair, open);
					if (i == stations.length) { 
						stations = resize(stations, i * 3);
					}
					stations[i] = L;
					i++;
				}
			}
			stations = resize(stations, i);
		} catch (FileNotFoundException e) {
			System.out.println("Wrong filepath,pls check and input.");
		} catch (Exception e) {
			System.out.println("Read Error!");
		}
		return stations;
	}
	
	// resize array function
	public static CTAStation[] resize(CTAStation[] stations, int c)
	{
		CTAStation[] T = new CTAStation[c];
		int limit = (stations.length < c) ? stations.length:c;
		for(int i=0; i<limit; i++) {
			T[i] = stations[i];
		}
		stations = T;
		T = null;
		return stations;
	}
	
	//Displays the menu options, which should be the following:
	//Display Station Names
	//Display Stations with/without Wheelchair access
	//Display Nearest Station
	//Exit
	//Performs the operation requested by the user
	//Loops until 'Exit' is chosen
	// Menu
		public static void menu(CTAStation[] stations) {
			Scanner input = new Scanner(System.in);
			boolean finish = true;
			do {
				System.out.println("1.Display Station Names");
				System.out.println("2.Display Stations with/without Wheelchair access");
				System.out.println("3.Display Nearest Station");
				System.out.println("4.Exit");
				System.out.println("what is your choice:");
				String choose = input.nextLine();
				switch (choose) {
					case "1":
						displayStationNames(stations);
						break;
					case "2":
						displayByWheelchair(input, stations);
						break;
					case "3":
						displayNearest(input, stations);
						break;
					case "4":
						finish = true;
						break;
					default:
						System.out.println("Error!!!");			
				}		
			} while(!finish);	
			System.out.println("Byebye!");
			input.close();
		}
		
		// display StationNames function
		public static void displayStationNames(CTAStation[] stations) {
			for (int i=0; i<stations.length; i++)
			{
				System.out.println("The name is: " + stations[i].getName());
			}
		}
		
		//displayByWheelchair:Prompts the user for accessibility ('y' or 'n')
		//Checks that the input is 'y' or 'n', continues to prompt the user for 'y' or 'n' until one has been entered
		//If char entered is a valid choice:Determine which boolean to use ('y' -> true, 'n' -> false)
		//Loop through the CTAStation[] to look at wheelchair and display CTAStations that meet the requirement
		//Display a message if no stations are found
		//display ByWheelchair function
		public static void displayByWheelchair(Scanner input, CTAStation[] stations) {
			System.out.println("Please input 'y' or 'n'(About With or Without Wheelchair):");
			boolean tag = false;
			boolean hasWheelchair = false;
			do {		
				char choose = input.nextLine().charAt(0);
				switch (choose) {
					case 'y':
						hasWheelchair = true;
						tag = true;
						break;
					case 'n':
						tag = true;
						break;
					default:
						System.out.println("Error, Please check then input again, please");
				}					
			} while(!tag);
			boolean k = true;
			int j = 0;
			for (int i=0; i<stations.length; i++)
			{
				if (stations[i].hasWheelchair() == hasWheelchair) {
					System.out.println(stations[i].toString());
					k = false;
					j++;
				}
			}
			if(k) {
				System.out.println("No stations are found.");
			}else {
				System.out.println(j + " stations are found.");
			}
		}
		
		//displayNearest:
		//Prompts the user for a latitude and longitude
		//Uses the values entered to iterate through the CTAStation[] to find the nearest station (using calcDistance) and displays it to the cons
		public static void displayNearest(Scanner input, CTAStation[] stations) {
			double lat = 1.0;
			double lng = 2.0;
			boolean finish = false;
			do {
				//System.out.println("please input the latitude:");
				try {
					System.out.println("please input the latitude:");
					String latx = input.nextLine();
					lat = Double.parseDouble(latx);
					System.out.println("longitude:");
					String lngy = input.nextLine();
					lng = Double.parseDouble(lngy);
					finish = true;
				} catch (Exception e) {
					System.out.println("Error, please input data!");
				}
			} while (!finish);
		   //double x1 = Math.cos(lat1) * Math.cos(lng1);
	       //double y1 = Math.cos(lat1) * Math.sin(lng1);
	       //double z1 = Math.sin(lat1);
	       //double x2 = Math.cos(lat2) * Math.cos(lng2);
           //double lineDistance =Math.sqrt((x1 - x2) * (x1 - x2)
			double neareststation[] = new double[stations.length];
		}
		
		
}
