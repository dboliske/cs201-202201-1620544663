# CS 201

Welcome to CS 201! This repository will contain all assignments, exams, and projects that you will be working on throughout the semester. There may not be much in here to start with, but more will be added as time goes on.

## Course Information

This course has additional details and resources available on the [course website](http://mypages.iit.edu/~dboliske) and in the [course lecture repository](https://github.com/dboliske/cs201-202201-lectures/src/main/).

## Questions?

For questions about grades on the homework assignments, please contact your TA, [Yimiao Zhang](mailto:yimiao.zhang@beaconedu.com), prior to discussing them with your professor.

If you have any issues using your repository, please message in the Discord or email me at [dboliske@hawk.iit.edu](mailto:dboliske@hawk.iit.edu) or talk to me or your TA after class or during lab.